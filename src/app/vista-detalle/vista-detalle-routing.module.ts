import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { VistaDetalleComponent } from "./vista-detalle.component";

const routes: Routes = [
    { path: "", component: VistaDetalleComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class VistaDetalleRoutingModule { }
