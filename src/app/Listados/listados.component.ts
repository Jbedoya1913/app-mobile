import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: "Listados",
    templateUrl: "./listados.component.html"
})
export class ListadosComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 1000); }
    
    ngOnInit(): void {
        this.doLater(() =>
            dialogs.action("Mensaje", "Cancelar!", ["Opcion 1", "Opcion 2"])
                .then((result) => {
                                    console.log("resultado: " + result);
                                    if (result === "Opcion 1") {
                                        this.doLater(() =>
                                            dialogs.alert({
                                                title: "Titulo 1 ",
                                                message: "msj 1",
                                                okButtonText: "btn 1"
                                            }).then(() => console.log("Cerrado 1")));
                                    } else if (result === "Opcion 2") {
                                        this.doLater(() =>
                                            dialogs.alert({
                                                title: "Titulo 2",
                                                message: "msj 2",
                                                okButtonText: "btn 2"
                                            }).then(() => console.log("Cerrado 2")));
                                    }
                }));
            }
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
