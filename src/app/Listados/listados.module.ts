import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ListadosRoutingModule } from "./listados-routing.module";
import { ListadosComponent } from "./listados.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ListadosRoutingModule
    ],
    declarations: [
        ListadosComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ListadosModule { }
